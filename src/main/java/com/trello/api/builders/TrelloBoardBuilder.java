package com.trello.api.builders;

import com.trello.api.models.*;

import java.util.List;

public class TrelloBoardBuilder {
    private TrelloBoard trelloBoard;

    public void createBoard() {
        trelloBoard = new TrelloBoard();
    }

    public TrelloBoardBuilder setId(String val) {
        trelloBoard.setId(val);
        return this;
    }

    public TrelloBoardBuilder setName(String val) {
        trelloBoard.setName(val);
        return this;
    }

    public TrelloBoardBuilder setDesc(String val) {
        trelloBoard.setDesc(val);
        return this;
    }

    public TrelloBoardBuilder setDescData(Object val) {
        trelloBoard.setDescData(val);
        return this;
    }

    public TrelloBoardBuilder setClosed(Boolean val) {
        trelloBoard.setClosed(val);
        return this;
    }

    public TrelloBoardBuilder setIdOrganization(Object val) {
        trelloBoard.setIdOrganization(val);
        return this;
    }

    public TrelloBoardBuilder setInvited(Boolean val) {
        trelloBoard.setInvited(val);
        return this;
    }

    public TrelloBoardBuilder setLimits(Limits val) {
        trelloBoard.setLimits(val);
        return this;
    }

    public TrelloBoardBuilder setMemberships(List<Membership> val) {
        trelloBoard.setMemberships(val);
        return this;
    }

    public TrelloBoardBuilder setPinned(Boolean val) {
        trelloBoard.setPinned(val);
        return this;
    }

    public TrelloBoardBuilder setStarred(Boolean val) {
        trelloBoard.setStarred(val);
        return this;
    }

    public TrelloBoardBuilder setUrl(String val) {
        trelloBoard.setUrl(val);
        return this;
    }

    public TrelloBoardBuilder setPrefs(Prefs val) {
        trelloBoard.setPrefs(val);
        return this;
    }

    public TrelloBoardBuilder setInvitations(List<Object> val) {
        trelloBoard.setInvitations(val);
        return this;
    }

    public TrelloBoardBuilder setShortLink(String val) {
        trelloBoard.setShortLink(val);
        return this;
    }

    public TrelloBoardBuilder setSubscribed(Boolean val) {
        trelloBoard.setSubscribed(val);
        return this;
    }

    public TrelloBoardBuilder setLabelNames(LabelNames val) {
        trelloBoard.setLabelNames(val);
        return this;
    }

    public TrelloBoardBuilder setPowerUps(List<Object> val) {
        trelloBoard.setPowerUps(val);
        return this;
    }

    public TrelloBoardBuilder setDateLastActivity(String val) {
        trelloBoard.setDateLastActivity(val);
        return this;
    }

    public TrelloBoardBuilder setDateLastView(String val) {
        trelloBoard.setDateLastView(val);
        return this;
    }

    public TrelloBoardBuilder setShortUrl(String val) {
        trelloBoard.setShortUrl(val);
        return this;
    }

    public TrelloBoardBuilder setIdTags(List<Object> val) {
        trelloBoard.setIdTags(val);
        return this;
    }

    public TrelloBoardBuilder setDatePluginDisable(Object val) {
        trelloBoard.setDatePluginDisable(val);
        return this;
    }

    public TrelloBoard build() {
        return trelloBoard;
    }
}
