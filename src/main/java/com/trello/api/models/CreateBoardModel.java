package com.trello.api.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.annotation.Generated;

@Generated("com.robohorse.robopojogenerator")
public class CreateBoardModel {

    @JsonProperty("name")
    private String name;

    @JsonProperty("defaultLists")
    private boolean defaultLists;

    private CreateBoardModel(Builder builder) {
        setName(builder.name);
        setDefaultLists(builder.defaultLists);
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setDefaultLists(boolean defaultLists) {
        this.defaultLists = defaultLists;
    }

    public boolean isDefaultLists() {
        return defaultLists;
    }

    @Override
    public String toString() {
        return
                "CreateBoardModel{" +
                        "name = '" + name + '\'' +
                        ",defaultLists = '" + defaultLists + '\'' +
                        "}";
    }

    public static final class Builder {
        private String name;
        private boolean defaultLists;

        public Builder() {
        }

        public Builder setName(String val) {
            name = val;
            return this;
        }

        public Builder setDefaultLists(boolean val) {
            defaultLists = val;
            return this;
        }

        public CreateBoardModel build() {
            return new CreateBoardModel(this);
        }
    }
}