import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "name",
    "defaultLists"
})
public class CreateBoard {

    @JsonProperty("name")
    private String name;
    @JsonProperty("defaultLists")
    private Boolean defaultLists;

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("defaultLists")
    public Boolean getDefaultLists() {
        return defaultLists;
    }

    @JsonProperty("defaultLists")
    public void setDefaultLists(Boolean defaultLists) {
        this.defaultLists = defaultLists;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(CreateBoard.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("name");
        sb.append('=');
        sb.append(((this.name == null)?"<null>":this.name));
        sb.append(',');
        sb.append("defaultLists");
        sb.append('=');
        sb.append(((this.defaultLists == null)?"<null>":this.defaultLists));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.name == null)? 0 :this.name.hashCode()));
        result = ((result* 31)+((this.defaultLists == null)? 0 :this.defaultLists.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof CreateBoard) == false) {
            return false;
        }
        CreateBoard rhs = ((CreateBoard) other);
        return (((this.name == rhs.name)||((this.name!= null)&&this.name.equals(rhs.name)))&&((this.defaultLists == rhs.defaultLists)||((this.defaultLists!= null)&&this.defaultLists.equals(rhs.defaultLists))));
    }

}
