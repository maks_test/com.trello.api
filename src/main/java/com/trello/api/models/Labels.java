
package com.trello.api.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "perBoard"
})
public class Labels {

    @JsonProperty("perBoard")
    private PerBoard perBoard;

    @JsonProperty("perBoard")
    public PerBoard getPerBoard() {
        return perBoard;
    }

    @JsonProperty("perBoard")
    public void setPerBoard(PerBoard perBoard) {
        this.perBoard = perBoard;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Labels.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("perBoard");
        sb.append('=');
        sb.append(((this.perBoard == null) ? "<null>" : this.perBoard));
        sb.append(',');
        if (sb.charAt((sb.length() - 1)) == ',') {
            sb.setCharAt((sb.length() - 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result * 31) + ((this.perBoard == null) ? 0 : this.perBoard.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Labels) == false) {
            return false;
        }
        Labels rhs = ((Labels) other);
        return ((this.perBoard == rhs.perBoard) || ((this.perBoard != null) && this.perBoard.equals(rhs.perBoard)));
    }

}
