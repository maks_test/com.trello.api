package com.trello.api.tests;

import com.trello.api.assertions.TrelloBoardAssert;
import com.trello.api.controllers.TrelloBoardController;
import com.trello.api.models.TrelloBoard;
import io.qameta.allure.Description;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.lang.RandomStringUtils;
import org.junit.Assert;
import org.junit.Test;

import static io.restassured.config.EncoderConfig.encoderConfig;

public class ObjectStyleTest {

    private String generateBoardName() {
        return "Board-" + RandomStringUtils.randomNumeric(4);
    }

    @Test
    @Description("/bCREATE NEW BOARD/b")
    public void createNewBoard() {
        String boardName = generateBoardName();
        TrelloBoard trelloBoard = new TrelloBoardController().createNewBoard(boardName);
        TrelloBoardAssert.assertThat(trelloBoard).hasName(boardName);
//        TrelloBoardAssert.assertThat(actualTrelloBoard).isEqualTo(expectedTrelloBoard);
    }

    @Test
    public void createAndDeleteBoard() {
        TrelloBoardController boardController = new TrelloBoardController();
        TrelloBoard trelloBoard = boardController.createNewBoard(generateBoardName());
        boardController.deleteBoard(trelloBoard);
        String response = boardController.getDeletedBoard(trelloBoard);
        Assert.assertEquals("Response text should be ", "The requested resource was not found.", response);
    }

    @Test
    public void createUpdateAndDeleteBoard() {
        TrelloBoardController boardController = new TrelloBoardController();
        TrelloBoard trelloBoard = boardController.createNewBoard(generateBoardName());
        String newName = generateBoardName();
        trelloBoard.setName(newName);
        TrelloBoard updatedBoard = boardController.updateBoard(trelloBoard);
        boardController.deleteBoard(trelloBoard);
        TrelloBoardAssert.assertThat(updatedBoard).hasName(newName);
//        TrelloBoardAssert.assertThat(trelloBoard).isEqualTo(updatedBoard);
    }

    @Test
    public void getDeletedBoard() {
        TrelloBoard trelloBoard = new TrelloBoard();
        trelloBoard.setId("24234234");
        String response = new TrelloBoardController().getDeletedBoard(trelloBoard);
        Assert.assertEquals("Response text should be ", "board not found", response);
    }

    @Test
    public void createSeveralBoardsAndDeleteAllBoards() {
        TrelloBoardController boardController = new TrelloBoardController();
        boardController.createNewBoard(generateBoardName());
        boardController.createNewBoard(generateBoardName());
        boardController.createNewBoard(generateBoardName());
        boardController.deleteAllBoards();
    }

    @Test
    public void oldCreateNewBoard() {
        String key = "baa0fcad2719638c39f2727007878daf";
        String token = "a0e61cdedd02cba4f7fe3d1496b3bb48232def78808a5fb519681d5fba49747b";

        RequestSpecification requestSpecification = new RequestSpecBuilder()
                .setBaseUri("https://api.trello.com/1")
                .setBasePath("board")
                .addQueryParam("key", key)
                .addQueryParam("token", token)
                .setAccept(ContentType.JSON)
                .setContentType(ContentType.JSON)
                .log(LogDetail.ALL)
                .build();

        RestAssured.given()
                .spec(requestSpecification)
                .config(RestAssured.config().encoderConfig(encoderConfig().encodeContentTypeAs("*/*", ContentType.ANY)))
                .body("{\n" +
                        "\"name\": \"Board\",\n" +
                        "\"defaultLists\": true\n" +
                        "}"
                )
                .when().post()
                .then()
                .statusCode(200)
                .extract().response().body()
                .prettyPrint();
//                .body("name", Matchers.equalTo(boardName));
    }
}
