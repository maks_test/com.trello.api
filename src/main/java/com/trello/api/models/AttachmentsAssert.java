package com.trello.api.models;

/**
 * {@link Attachments} specific assertions - Generated by CustomAssertionGenerator.
 * <p>
 * Although this class is not final to allow Soft assertions proxy, if you wish to extend it,
 * extend {@link AbstractAttachmentsAssert} instead.
 */
@javax.annotation.Generated(value = "assertj-assertions-generator")
public class AttachmentsAssert extends AbstractAttachmentsAssert<AttachmentsAssert, Attachments> {

    /**
     * Creates a new <code>{@link AttachmentsAssert}</code> to make assertions on actual Attachments.
     *
     * @param actual the Attachments we want to make assertions on.
     */
    public AttachmentsAssert(Attachments actual) {
        super(actual, AttachmentsAssert.class);
    }

    /**
     * An entry point for AttachmentsAssert to follow AssertJ standard <code>assertThat()</code> statements.<br>
     * With a static import, one can write directly: <code>assertThat(myAttachments)</code> and get specific assertion with code completion.
     *
     * @param actual the Attachments we want to make assertions on.
     * @return a new <code>{@link AttachmentsAssert}</code>
     */
    @org.assertj.core.util.CheckReturnValue
    public static AttachmentsAssert assertThat(Attachments actual) {
        return new AttachmentsAssert(actual);
    }
}
