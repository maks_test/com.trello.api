package com.trello.api.assertions;

import com.trello.api.models.Attachments;
import org.assertj.core.api.AbstractObjectAssert;
import org.assertj.core.util.Objects;

/**
 * Abstract base class for {@link Attachments} specific assertions - Generated by CustomAssertionGenerator.
 */
@javax.annotation.Generated(value = "assertj-assertions-generator")
public abstract class AbstractAttachmentsAssert<S extends AbstractAttachmentsAssert<S, A>, A extends Attachments> extends AbstractObjectAssert<S, A> {

  /**
   * Creates a new <code>{@link AbstractAttachmentsAssert}</code> to make assertions on actual Attachments.
   *
   * @param actual the Attachments we want to make assertions on.
   */
  protected AbstractAttachmentsAssert(A actual, Class<S> selfType) {
    super(actual, selfType);
  }

  /**
   * Verifies that the actual Attachments's perBoard is equal to the given one.
   *
   * @param perBoard the given perBoard to compare the actual Attachments's perBoard to.
   * @return this assertion object.
   * @throws AssertionError - if the actual Attachments's perBoard is not equal to the given one.
   */
  public S hasPerBoard(com.trello.api.models.PerBoard perBoard) {
    // check that actual Attachments we want to make assertions on is not null.
    isNotNull();

    // overrides the default error message with a more explicit one
    String assertjErrorMessage = "\nExpecting perBoard of:\n  <%s>\nto be:\n  <%s>\nbut was:\n  <%s>";

    // null safe check
    com.trello.api.models.PerBoard actualPerBoard = actual.getPerBoard();
    if (!Objects.areEqual(actualPerBoard, perBoard)) {
      failWithMessage(assertjErrorMessage, actual, perBoard, actualPerBoard);
    }

    // return the current assertion for method chaining
    return myself;
  }

}
