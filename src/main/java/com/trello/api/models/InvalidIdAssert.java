package com.trello.api.models;

/**
 * {@link InvalidId} specific assertions - Generated by CustomAssertionGenerator.
 * <p>
 * Although this class is not final to allow Soft assertions proxy, if you wish to extend it,
 * extend {@link AbstractInvalidIdAssert} instead.
 */
@javax.annotation.Generated(value = "assertj-assertions-generator")
public class InvalidIdAssert extends AbstractInvalidIdAssert<InvalidIdAssert, InvalidId> {

    /**
     * Creates a new <code>{@link InvalidIdAssert}</code> to make assertions on actual InvalidId.
     *
     * @param actual the InvalidId we want to make assertions on.
     */
    public InvalidIdAssert(InvalidId actual) {
        super(actual, InvalidIdAssert.class);
    }

    /**
     * An entry point for InvalidIdAssert to follow AssertJ standard <code>assertThat()</code> statements.<br>
     * With a static import, one can write directly: <code>assertThat(myInvalidId)</code> and get specific assertion with code completion.
     *
     * @param actual the InvalidId we want to make assertions on.
     * @return a new <code>{@link InvalidIdAssert}</code>
     */
    @org.assertj.core.util.CheckReturnValue
    public static InvalidIdAssert assertThat(InvalidId actual) {
        return new InvalidIdAssert(actual);
    }
}
