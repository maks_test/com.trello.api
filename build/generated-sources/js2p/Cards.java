import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "openPerBoard",
    "totalPerBoard"
})
public class Cards {

    @JsonProperty("openPerBoard")
    private OpenPerBoard openPerBoard;
    @JsonProperty("totalPerBoard")
    private TotalPerBoard totalPerBoard;

    @JsonProperty("openPerBoard")
    public OpenPerBoard getOpenPerBoard() {
        return openPerBoard;
    }

    @JsonProperty("openPerBoard")
    public void setOpenPerBoard(OpenPerBoard openPerBoard) {
        this.openPerBoard = openPerBoard;
    }

    @JsonProperty("totalPerBoard")
    public TotalPerBoard getTotalPerBoard() {
        return totalPerBoard;
    }

    @JsonProperty("totalPerBoard")
    public void setTotalPerBoard(TotalPerBoard totalPerBoard) {
        this.totalPerBoard = totalPerBoard;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Cards.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("openPerBoard");
        sb.append('=');
        sb.append(((this.openPerBoard == null)?"<null>":this.openPerBoard));
        sb.append(',');
        sb.append("totalPerBoard");
        sb.append('=');
        sb.append(((this.totalPerBoard == null)?"<null>":this.totalPerBoard));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.totalPerBoard == null)? 0 :this.totalPerBoard.hashCode()));
        result = ((result* 31)+((this.openPerBoard == null)? 0 :this.openPerBoard.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Cards) == false) {
            return false;
        }
        Cards rhs = ((Cards) other);
        return (((this.totalPerBoard == rhs.totalPerBoard)||((this.totalPerBoard!= null)&&this.totalPerBoard.equals(rhs.totalPerBoard)))&&((this.openPerBoard == rhs.openPerBoard)||((this.openPerBoard!= null)&&this.openPerBoard.equals(rhs.openPerBoard))));
    }

}
