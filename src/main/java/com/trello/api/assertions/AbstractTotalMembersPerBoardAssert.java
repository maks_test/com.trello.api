package com.trello.api.assertions;

import com.trello.api.models.TotalMembersPerBoard;
import org.assertj.core.api.AbstractObjectAssert;
import org.assertj.core.util.Objects;

/**
 * Abstract base class for {@link TotalMembersPerBoard} specific assertions - Generated by CustomAssertionGenerator.
 */
@javax.annotation.Generated(value = "assertj-assertions-generator")
public abstract class AbstractTotalMembersPerBoardAssert<S extends AbstractTotalMembersPerBoardAssert<S, A>, A extends TotalMembersPerBoard> extends AbstractObjectAssert<S, A> {

    /**
     * Creates a new <code>{@link AbstractTotalMembersPerBoardAssert}</code> to make assertions on actual TotalMembersPerBoard.
     *
     * @param actual the TotalMembersPerBoard we want to make assertions on.
     */
    protected AbstractTotalMembersPerBoardAssert(A actual, Class<S> selfType) {
        super(actual, selfType);
    }

    /**
     * Verifies that the actual TotalMembersPerBoard's disableAt is equal to the given one.
     *
     * @param disableAt the given disableAt to compare the actual TotalMembersPerBoard's disableAt to.
     * @return this assertion object.
     * @throws AssertionError - if the actual TotalMembersPerBoard's disableAt is not equal to the given one.
     */
    public S hasDisableAt(Integer disableAt) {
        // check that actual TotalMembersPerBoard we want to make assertions on is not null.
        isNotNull();

        // overrides the default error message with a more explicit one
        String assertjErrorMessage = "\nExpecting disableAt of:\n  <%s>\nto be:\n  <%s>\nbut was:\n  <%s>";

        // null safe check
        Integer actualDisableAt = actual.getDisableAt();
        if (!Objects.areEqual(actualDisableAt, disableAt)) {
            failWithMessage(assertjErrorMessage, actual, disableAt, actualDisableAt);
        }

        // return the current assertion for method chaining
        return myself;
    }

    /**
     * Verifies that the actual TotalMembersPerBoard's status is equal to the given one.
     *
     * @param status the given status to compare the actual TotalMembersPerBoard's status to.
     * @return this assertion object.
     * @throws AssertionError - if the actual TotalMembersPerBoard's status is not equal to the given one.
     */
    public S hasStatus(String status) {
        // check that actual TotalMembersPerBoard we want to make assertions on is not null.
        isNotNull();

        // overrides the default error message with a more explicit one
        String assertjErrorMessage = "\nExpecting status of:\n  <%s>\nto be:\n  <%s>\nbut was:\n  <%s>";

        // null safe check
        String actualStatus = actual.getStatus();
        if (!Objects.areEqual(actualStatus, status)) {
            failWithMessage(assertjErrorMessage, actual, status, actualStatus);
        }

        // return the current assertion for method chaining
        return myself;
    }

    /**
     * Verifies that the actual TotalMembersPerBoard's warnAt is equal to the given one.
     *
     * @param warnAt the given warnAt to compare the actual TotalMembersPerBoard's warnAt to.
     * @return this assertion object.
     * @throws AssertionError - if the actual TotalMembersPerBoard's warnAt is not equal to the given one.
     */
    public S hasWarnAt(Integer warnAt) {
        // check that actual TotalMembersPerBoard we want to make assertions on is not null.
        isNotNull();

        // overrides the default error message with a more explicit one
        String assertjErrorMessage = "\nExpecting warnAt of:\n  <%s>\nto be:\n  <%s>\nbut was:\n  <%s>";

        // null safe check
        Integer actualWarnAt = actual.getWarnAt();
        if (!Objects.areEqual(actualWarnAt, warnAt)) {
            failWithMessage(assertjErrorMessage, actual, warnAt, actualWarnAt);
        }

        // return the current assertion for method chaining
        return myself;
    }

}
