
package com.trello.api.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "attachments",
        "boards",
        "cards",
        "checklists",
        "customFields",
        "labels",
        "lists"
})
public class Limits {

    @JsonProperty("attachments")
    private Attachments attachments;
    @JsonProperty("boards")
    private Boards boards;
    @JsonProperty("cards")
    private Cards cards;
    @JsonProperty("checklists")
    private Checklists checklists;
    @JsonProperty("customFields")
    private CustomFields customFields;
    @JsonProperty("labels")
    private Labels labels;
    @JsonProperty("lists")
    private Lists lists;

    @JsonProperty("attachments")
    public Attachments getAttachments() {
        return attachments;
    }

    @JsonProperty("attachments")
    public void setAttachments(Attachments attachments) {
        this.attachments = attachments;
    }

    @JsonProperty("boards")
    public Boards getBoards() {
        return boards;
    }

    @JsonProperty("boards")
    public void setBoards(Boards boards) {
        this.boards = boards;
    }

    @JsonProperty("cards")
    public Cards getCards() {
        return cards;
    }

    @JsonProperty("cards")
    public void setCards(Cards cards) {
        this.cards = cards;
    }

    @JsonProperty("checklists")
    public Checklists getChecklists() {
        return checklists;
    }

    @JsonProperty("checklists")
    public void setChecklists(Checklists checklists) {
        this.checklists = checklists;
    }

    @JsonProperty("customFields")
    public CustomFields getCustomFields() {
        return customFields;
    }

    @JsonProperty("customFields")
    public void setCustomFields(CustomFields customFields) {
        this.customFields = customFields;
    }

    @JsonProperty("labels")
    public Labels getLabels() {
        return labels;
    }

    @JsonProperty("labels")
    public void setLabels(Labels labels) {
        this.labels = labels;
    }

    @JsonProperty("lists")
    public Lists getLists() {
        return lists;
    }

    @JsonProperty("lists")
    public void setLists(Lists lists) {
        this.lists = lists;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Limits.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("attachments");
        sb.append('=');
        sb.append(((this.attachments == null) ? "<null>" : this.attachments));
        sb.append(',');
        sb.append("boards");
        sb.append('=');
        sb.append(((this.boards == null) ? "<null>" : this.boards));
        sb.append(',');
        sb.append("cards");
        sb.append('=');
        sb.append(((this.cards == null) ? "<null>" : this.cards));
        sb.append(',');
        sb.append("checklists");
        sb.append('=');
        sb.append(((this.checklists == null) ? "<null>" : this.checklists));
        sb.append(',');
        sb.append("customFields");
        sb.append('=');
        sb.append(((this.customFields == null) ? "<null>" : this.customFields));
        sb.append(',');
        sb.append("labels");
        sb.append('=');
        sb.append(((this.labels == null) ? "<null>" : this.labels));
        sb.append(',');
        sb.append("lists");
        sb.append('=');
        sb.append(((this.lists == null) ? "<null>" : this.lists));
        sb.append(',');
        if (sb.charAt((sb.length() - 1)) == ',') {
            sb.setCharAt((sb.length() - 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result * 31) + ((this.checklists == null) ? 0 : this.checklists.hashCode()));
        result = ((result * 31) + ((this.attachments == null) ? 0 : this.attachments.hashCode()));
        result = ((result * 31) + ((this.cards == null) ? 0 : this.cards.hashCode()));
        result = ((result * 31) + ((this.customFields == null) ? 0 : this.customFields.hashCode()));
        result = ((result * 31) + ((this.lists == null) ? 0 : this.lists.hashCode()));
        result = ((result * 31) + ((this.boards == null) ? 0 : this.boards.hashCode()));
        result = ((result * 31) + ((this.labels == null) ? 0 : this.labels.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Limits) == false) {
            return false;
        }
        Limits rhs = ((Limits) other);
        return ((((((((this.checklists == rhs.checklists) || ((this.checklists != null) && this.checklists.equals(rhs.checklists))) && ((this.attachments == rhs.attachments) || ((this.attachments != null) && this.attachments.equals(rhs.attachments)))) && ((this.cards == rhs.cards) || ((this.cards != null) && this.cards.equals(rhs.cards)))) && ((this.customFields == rhs.customFields) || ((this.customFields != null) && this.customFields.equals(rhs.customFields)))) && ((this.lists == rhs.lists) || ((this.lists != null) && this.lists.equals(rhs.lists)))) && ((this.boards == rhs.boards) || ((this.boards != null) && this.boards.equals(rhs.boards)))) && ((this.labels == rhs.labels) || ((this.labels != null) && this.labels.equals(rhs.labels))));
    }

}
