package com.trello.api.models;

public class InvalidId {
    private String invalidId;

    public String getInvalidId() {
        return invalidId;
    }

    public void setInvalidId(String invalidId) {
        this.invalidId = invalidId;
    }
}
