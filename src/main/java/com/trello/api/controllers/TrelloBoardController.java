package com.trello.api.controllers;

import com.trello.api.models.CreateBoardModel;
import com.trello.api.models.TrelloBoard;
import io.qameta.allure.Step;
import io.restassured.RestAssured;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import org.hamcrest.Matchers;

import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.given;

public class TrelloBoardController extends BaseController {
    private List<TrelloBoard> trelloBoardList = new ArrayList<>();

    @Step
    public TrelloBoard createNewBoard(String boardName) {
        TrelloBoard trelloBoard = given(requestSpecification)
                .body(createBoardModel(boardName))
                .post()
                .as(TrelloBoard.class);
        trelloBoardList.add(trelloBoard);
        return trelloBoard;
    }

    @Step
    public TrelloBoard getBoard(TrelloBoard trelloBoard) {
        return given(requestSpecification)
                .get(trelloBoard.getId())
                .as(TrelloBoard.class);
    }

    @Step
    public TrelloBoard updateBoard(TrelloBoard trelloBoard) {
        return given(requestSpecification)
                .body(trelloBoard)
                .put(trelloBoard.getId())
                .as(TrelloBoard.class);
    }

    @Step
    public void deleteBoard(TrelloBoard trelloBoard) {
        given(requestSpecification)
                .delete(trelloBoard.getId())
                .then()
                .statusCode(200)
                .body("_value", Matchers.nullValue());
    }

    @Step
    public String getDeletedBoard(TrelloBoard trelloBoard) {
        RestAssured.responseSpecification = new ResponseSpecBuilder()
                .expectContentType(ContentType.TEXT)
                .expectResponseTime(Matchers.lessThan(15000L))
                .build();
        return given(requestSpecification)
                .get(trelloBoard.getId())
                .body()
                .prettyPrint();
    }

    @Step
    public void deleteAllBoards() {
        trelloBoardList.forEach(this::deleteBoard);
    }

    @Step
    private CreateBoardModel createBoardModel(String boardName) {
        return new CreateBoardModel.Builder()
                .setName(boardName)
                .setDefaultLists(true)
                .build();
    }
}
