package com.trello.api.assertions;

import com.trello.api.models.Limits;
import org.assertj.core.api.AbstractObjectAssert;
import org.assertj.core.util.Objects;

/**
 * Abstract base class for {@link Limits} specific assertions - Generated by CustomAssertionGenerator.
 */
@javax.annotation.Generated(value = "assertj-assertions-generator")
public abstract class AbstractLimitsAssert<S extends AbstractLimitsAssert<S, A>, A extends Limits> extends AbstractObjectAssert<S, A> {

    /**
     * Creates a new <code>{@link AbstractLimitsAssert}</code> to make assertions on actual Limits.
     *
     * @param actual the Limits we want to make assertions on.
     */
    protected AbstractLimitsAssert(A actual, Class<S> selfType) {
        super(actual, selfType);
    }

    /**
     * Verifies that the actual Limits's attachments is equal to the given one.
     *
     * @param attachments the given attachments to compare the actual Limits's attachments to.
     * @return this assertion object.
     * @throws AssertionError - if the actual Limits's attachments is not equal to the given one.
     */
    public S hasAttachments(com.trello.api.models.Attachments attachments) {
        // check that actual Limits we want to make assertions on is not null.
        isNotNull();

        // overrides the default error message with a more explicit one
        String assertjErrorMessage = "\nExpecting attachments of:\n  <%s>\nto be:\n  <%s>\nbut was:\n  <%s>";

        // null safe check
        com.trello.api.models.Attachments actualAttachments = actual.getAttachments();
        if (!Objects.areEqual(actualAttachments, attachments)) {
            failWithMessage(assertjErrorMessage, actual, attachments, actualAttachments);
        }

        // return the current assertion for method chaining
        return myself;
    }

    /**
     * Verifies that the actual Limits's boards is equal to the given one.
     *
     * @param boards the given boards to compare the actual Limits's boards to.
     * @return this assertion object.
     * @throws AssertionError - if the actual Limits's boards is not equal to the given one.
     */
    public S hasBoards(com.trello.api.models.Boards boards) {
        // check that actual Limits we want to make assertions on is not null.
        isNotNull();

        // overrides the default error message with a more explicit one
        String assertjErrorMessage = "\nExpecting boards of:\n  <%s>\nto be:\n  <%s>\nbut was:\n  <%s>";

        // null safe check
        com.trello.api.models.Boards actualBoards = actual.getBoards();
        if (!Objects.areEqual(actualBoards, boards)) {
            failWithMessage(assertjErrorMessage, actual, boards, actualBoards);
        }

        // return the current assertion for method chaining
        return myself;
    }

    /**
     * Verifies that the actual Limits's cards is equal to the given one.
     *
     * @param cards the given cards to compare the actual Limits's cards to.
     * @return this assertion object.
     * @throws AssertionError - if the actual Limits's cards is not equal to the given one.
     */
    public S hasCards(com.trello.api.models.Cards cards) {
        // check that actual Limits we want to make assertions on is not null.
        isNotNull();

        // overrides the default error message with a more explicit one
        String assertjErrorMessage = "\nExpecting cards of:\n  <%s>\nto be:\n  <%s>\nbut was:\n  <%s>";

        // null safe check
        com.trello.api.models.Cards actualCards = actual.getCards();
        if (!Objects.areEqual(actualCards, cards)) {
            failWithMessage(assertjErrorMessage, actual, cards, actualCards);
        }

        // return the current assertion for method chaining
        return myself;
    }

    /**
     * Verifies that the actual Limits's checklists is equal to the given one.
     *
     * @param checklists the given checklists to compare the actual Limits's checklists to.
     * @return this assertion object.
     * @throws AssertionError - if the actual Limits's checklists is not equal to the given one.
     */
    public S hasChecklists(com.trello.api.models.Checklists checklists) {
        // check that actual Limits we want to make assertions on is not null.
        isNotNull();

        // overrides the default error message with a more explicit one
        String assertjErrorMessage = "\nExpecting checklists of:\n  <%s>\nto be:\n  <%s>\nbut was:\n  <%s>";

        // null safe check
        com.trello.api.models.Checklists actualChecklists = actual.getChecklists();
        if (!Objects.areEqual(actualChecklists, checklists)) {
            failWithMessage(assertjErrorMessage, actual, checklists, actualChecklists);
        }

        // return the current assertion for method chaining
        return myself;
    }

    /**
     * Verifies that the actual Limits's customFields is equal to the given one.
     *
     * @param customFields the given customFields to compare the actual Limits's customFields to.
     * @return this assertion object.
     * @throws AssertionError - if the actual Limits's customFields is not equal to the given one.
     */
    public S hasCustomFields(com.trello.api.models.CustomFields customFields) {
        // check that actual Limits we want to make assertions on is not null.
        isNotNull();

        // overrides the default error message with a more explicit one
        String assertjErrorMessage = "\nExpecting customFields of:\n  <%s>\nto be:\n  <%s>\nbut was:\n  <%s>";

        // null safe check
        com.trello.api.models.CustomFields actualCustomFields = actual.getCustomFields();
        if (!Objects.areEqual(actualCustomFields, customFields)) {
            failWithMessage(assertjErrorMessage, actual, customFields, actualCustomFields);
        }

        // return the current assertion for method chaining
        return myself;
    }

    /**
     * Verifies that the actual Limits's labels is equal to the given one.
     *
     * @param labels the given labels to compare the actual Limits's labels to.
     * @return this assertion object.
     * @throws AssertionError - if the actual Limits's labels is not equal to the given one.
     */
    public S hasLabels(com.trello.api.models.Labels labels) {
        // check that actual Limits we want to make assertions on is not null.
        isNotNull();

        // overrides the default error message with a more explicit one
        String assertjErrorMessage = "\nExpecting labels of:\n  <%s>\nto be:\n  <%s>\nbut was:\n  <%s>";

        // null safe check
        com.trello.api.models.Labels actualLabels = actual.getLabels();
        if (!Objects.areEqual(actualLabels, labels)) {
            failWithMessage(assertjErrorMessage, actual, labels, actualLabels);
        }

        // return the current assertion for method chaining
        return myself;
    }

    /**
     * Verifies that the actual Limits's lists is equal to the given one.
     *
     * @param lists the given lists to compare the actual Limits's lists to.
     * @return this assertion object.
     * @throws AssertionError - if the actual Limits's lists is not equal to the given one.
     */
    public S hasLists(com.trello.api.models.Lists lists) {
        // check that actual Limits we want to make assertions on is not null.
        isNotNull();

        // overrides the default error message with a more explicit one
        String assertjErrorMessage = "\nExpecting lists of:\n  <%s>\nto be:\n  <%s>\nbut was:\n  <%s>";

        // null safe check
        com.trello.api.models.Lists actualLists = actual.getLists();
        if (!Objects.areEqual(actualLists, lists)) {
            failWithMessage(assertjErrorMessage, actual, lists, actualLists);
        }

        // return the current assertion for method chaining
        return myself;
    }

}
