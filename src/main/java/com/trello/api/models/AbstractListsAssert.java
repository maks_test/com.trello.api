package com.trello.api.models;

import org.assertj.core.api.AbstractObjectAssert;
import org.assertj.core.util.Objects;

/**
 * Abstract base class for {@link Lists} specific assertions - Generated by CustomAssertionGenerator.
 */
@javax.annotation.Generated(value = "assertj-assertions-generator")
public abstract class AbstractListsAssert<S extends AbstractListsAssert<S, A>, A extends Lists> extends AbstractObjectAssert<S, A> {

    /**
     * Creates a new <code>{@link AbstractListsAssert}</code> to make assertions on actual Lists.
     *
     * @param actual the Lists we want to make assertions on.
     */
    protected AbstractListsAssert(A actual, Class<S> selfType) {
        super(actual, selfType);
    }

    /**
     * Verifies that the actual Lists's openPerBoard is equal to the given one.
     *
     * @param openPerBoard the given openPerBoard to compare the actual Lists's openPerBoard to.
     * @return this assertion object.
     * @throws AssertionError - if the actual Lists's openPerBoard is not equal to the given one.
     */
    public S hasOpenPerBoard(OpenPerBoard openPerBoard) {
        // check that actual Lists we want to make assertions on is not null.
        isNotNull();

        // overrides the default error message with a more explicit one
        String assertjErrorMessage = "\nExpecting openPerBoard of:\n  <%s>\nto be:\n  <%s>\nbut was:\n  <%s>";

        // null safe check
        OpenPerBoard actualOpenPerBoard = actual.getOpenPerBoard();
        if (!Objects.areEqual(actualOpenPerBoard, openPerBoard)) {
            failWithMessage(assertjErrorMessage, actual, openPerBoard, actualOpenPerBoard);
        }

        // return the current assertion for method chaining
        return myself;
    }

    /**
     * Verifies that the actual Lists's totalPerBoard is equal to the given one.
     *
     * @param totalPerBoard the given totalPerBoard to compare the actual Lists's totalPerBoard to.
     * @return this assertion object.
     * @throws AssertionError - if the actual Lists's totalPerBoard is not equal to the given one.
     */
    public S hasTotalPerBoard(TotalPerBoard totalPerBoard) {
        // check that actual Lists we want to make assertions on is not null.
        isNotNull();

        // overrides the default error message with a more explicit one
        String assertjErrorMessage = "\nExpecting totalPerBoard of:\n  <%s>\nto be:\n  <%s>\nbut was:\n  <%s>";

        // null safe check
        TotalPerBoard actualTotalPerBoard = actual.getTotalPerBoard();
        if (!Objects.areEqual(actualTotalPerBoard, totalPerBoard)) {
            failWithMessage(assertjErrorMessage, actual, totalPerBoard, actualTotalPerBoard);
        }

        // return the current assertion for method chaining
        return myself;
    }

}
