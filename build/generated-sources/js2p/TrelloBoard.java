import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "id",
    "name",
    "desc",
    "descData",
    "closed",
    "idOrganization",
    "invited",
    "limits",
    "memberships",
    "pinned",
    "starred",
    "url",
    "prefs",
    "invitations",
    "shortLink",
    "subscribed",
    "labelNames",
    "powerUps",
    "dateLastActivity",
    "dateLastView",
    "shortUrl",
    "idTags",
    "datePluginDisable"
})
public class TrelloBoard {

    @JsonProperty("id")
    private String id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("desc")
    private String desc;
    @JsonProperty("descData")
    private Object descData;
    @JsonProperty("closed")
    private Boolean closed;
    @JsonProperty("idOrganization")
    private String idOrganization;
    @JsonProperty("invited")
    private Boolean invited;
    @JsonProperty("limits")
    private Limits limits;
    @JsonProperty("memberships")
    private List<Membership> memberships = new ArrayList<Membership>();
    @JsonProperty("pinned")
    private Boolean pinned;
    @JsonProperty("starred")
    private Boolean starred;
    @JsonProperty("url")
    private String url;
    @JsonProperty("prefs")
    private Prefs prefs;
    @JsonProperty("invitations")
    private List<Object> invitations = new ArrayList<Object>();
    @JsonProperty("shortLink")
    private String shortLink;
    @JsonProperty("subscribed")
    private Boolean subscribed;
    @JsonProperty("labelNames")
    private LabelNames labelNames;
    @JsonProperty("powerUps")
    private List<Object> powerUps = new ArrayList<Object>();
    @JsonProperty("dateLastActivity")
    private String dateLastActivity;
    @JsonProperty("dateLastView")
    private String dateLastView;
    @JsonProperty("shortUrl")
    private String shortUrl;
    @JsonProperty("idTags")
    private List<Object> idTags = new ArrayList<Object>();
    @JsonProperty("datePluginDisable")
    private Object datePluginDisable;

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("desc")
    public String getDesc() {
        return desc;
    }

    @JsonProperty("desc")
    public void setDesc(String desc) {
        this.desc = desc;
    }

    @JsonProperty("descData")
    public Object getDescData() {
        return descData;
    }

    @JsonProperty("descData")
    public void setDescData(Object descData) {
        this.descData = descData;
    }

    @JsonProperty("closed")
    public Boolean getClosed() {
        return closed;
    }

    @JsonProperty("closed")
    public void setClosed(Boolean closed) {
        this.closed = closed;
    }

    @JsonProperty("idOrganization")
    public String getIdOrganization() {
        return idOrganization;
    }

    @JsonProperty("idOrganization")
    public void setIdOrganization(String idOrganization) {
        this.idOrganization = idOrganization;
    }

    @JsonProperty("invited")
    public Boolean getInvited() {
        return invited;
    }

    @JsonProperty("invited")
    public void setInvited(Boolean invited) {
        this.invited = invited;
    }

    @JsonProperty("limits")
    public Limits getLimits() {
        return limits;
    }

    @JsonProperty("limits")
    public void setLimits(Limits limits) {
        this.limits = limits;
    }

    @JsonProperty("memberships")
    public List<Membership> getMemberships() {
        return memberships;
    }

    @JsonProperty("memberships")
    public void setMemberships(List<Membership> memberships) {
        this.memberships = memberships;
    }

    @JsonProperty("pinned")
    public Boolean getPinned() {
        return pinned;
    }

    @JsonProperty("pinned")
    public void setPinned(Boolean pinned) {
        this.pinned = pinned;
    }

    @JsonProperty("starred")
    public Boolean getStarred() {
        return starred;
    }

    @JsonProperty("starred")
    public void setStarred(Boolean starred) {
        this.starred = starred;
    }

    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    @JsonProperty("prefs")
    public Prefs getPrefs() {
        return prefs;
    }

    @JsonProperty("prefs")
    public void setPrefs(Prefs prefs) {
        this.prefs = prefs;
    }

    @JsonProperty("invitations")
    public List<Object> getInvitations() {
        return invitations;
    }

    @JsonProperty("invitations")
    public void setInvitations(List<Object> invitations) {
        this.invitations = invitations;
    }

    @JsonProperty("shortLink")
    public String getShortLink() {
        return shortLink;
    }

    @JsonProperty("shortLink")
    public void setShortLink(String shortLink) {
        this.shortLink = shortLink;
    }

    @JsonProperty("subscribed")
    public Boolean getSubscribed() {
        return subscribed;
    }

    @JsonProperty("subscribed")
    public void setSubscribed(Boolean subscribed) {
        this.subscribed = subscribed;
    }

    @JsonProperty("labelNames")
    public LabelNames getLabelNames() {
        return labelNames;
    }

    @JsonProperty("labelNames")
    public void setLabelNames(LabelNames labelNames) {
        this.labelNames = labelNames;
    }

    @JsonProperty("powerUps")
    public List<Object> getPowerUps() {
        return powerUps;
    }

    @JsonProperty("powerUps")
    public void setPowerUps(List<Object> powerUps) {
        this.powerUps = powerUps;
    }

    @JsonProperty("dateLastActivity")
    public String getDateLastActivity() {
        return dateLastActivity;
    }

    @JsonProperty("dateLastActivity")
    public void setDateLastActivity(String dateLastActivity) {
        this.dateLastActivity = dateLastActivity;
    }

    @JsonProperty("dateLastView")
    public String getDateLastView() {
        return dateLastView;
    }

    @JsonProperty("dateLastView")
    public void setDateLastView(String dateLastView) {
        this.dateLastView = dateLastView;
    }

    @JsonProperty("shortUrl")
    public String getShortUrl() {
        return shortUrl;
    }

    @JsonProperty("shortUrl")
    public void setShortUrl(String shortUrl) {
        this.shortUrl = shortUrl;
    }

    @JsonProperty("idTags")
    public List<Object> getIdTags() {
        return idTags;
    }

    @JsonProperty("idTags")
    public void setIdTags(List<Object> idTags) {
        this.idTags = idTags;
    }

    @JsonProperty("datePluginDisable")
    public Object getDatePluginDisable() {
        return datePluginDisable;
    }

    @JsonProperty("datePluginDisable")
    public void setDatePluginDisable(Object datePluginDisable) {
        this.datePluginDisable = datePluginDisable;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(TrelloBoard.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("id");
        sb.append('=');
        sb.append(((this.id == null)?"<null>":this.id));
        sb.append(',');
        sb.append("name");
        sb.append('=');
        sb.append(((this.name == null)?"<null>":this.name));
        sb.append(',');
        sb.append("desc");
        sb.append('=');
        sb.append(((this.desc == null)?"<null>":this.desc));
        sb.append(',');
        sb.append("descData");
        sb.append('=');
        sb.append(((this.descData == null)?"<null>":this.descData));
        sb.append(',');
        sb.append("closed");
        sb.append('=');
        sb.append(((this.closed == null)?"<null>":this.closed));
        sb.append(',');
        sb.append("idOrganization");
        sb.append('=');
        sb.append(((this.idOrganization == null)?"<null>":this.idOrganization));
        sb.append(',');
        sb.append("invited");
        sb.append('=');
        sb.append(((this.invited == null)?"<null>":this.invited));
        sb.append(',');
        sb.append("limits");
        sb.append('=');
        sb.append(((this.limits == null)?"<null>":this.limits));
        sb.append(',');
        sb.append("memberships");
        sb.append('=');
        sb.append(((this.memberships == null)?"<null>":this.memberships));
        sb.append(',');
        sb.append("pinned");
        sb.append('=');
        sb.append(((this.pinned == null)?"<null>":this.pinned));
        sb.append(',');
        sb.append("starred");
        sb.append('=');
        sb.append(((this.starred == null)?"<null>":this.starred));
        sb.append(',');
        sb.append("url");
        sb.append('=');
        sb.append(((this.url == null)?"<null>":this.url));
        sb.append(',');
        sb.append("prefs");
        sb.append('=');
        sb.append(((this.prefs == null)?"<null>":this.prefs));
        sb.append(',');
        sb.append("invitations");
        sb.append('=');
        sb.append(((this.invitations == null)?"<null>":this.invitations));
        sb.append(',');
        sb.append("shortLink");
        sb.append('=');
        sb.append(((this.shortLink == null)?"<null>":this.shortLink));
        sb.append(',');
        sb.append("subscribed");
        sb.append('=');
        sb.append(((this.subscribed == null)?"<null>":this.subscribed));
        sb.append(',');
        sb.append("labelNames");
        sb.append('=');
        sb.append(((this.labelNames == null)?"<null>":this.labelNames));
        sb.append(',');
        sb.append("powerUps");
        sb.append('=');
        sb.append(((this.powerUps == null)?"<null>":this.powerUps));
        sb.append(',');
        sb.append("dateLastActivity");
        sb.append('=');
        sb.append(((this.dateLastActivity == null)?"<null>":this.dateLastActivity));
        sb.append(',');
        sb.append("dateLastView");
        sb.append('=');
        sb.append(((this.dateLastView == null)?"<null>":this.dateLastView));
        sb.append(',');
        sb.append("shortUrl");
        sb.append('=');
        sb.append(((this.shortUrl == null)?"<null>":this.shortUrl));
        sb.append(',');
        sb.append("idTags");
        sb.append('=');
        sb.append(((this.idTags == null)?"<null>":this.idTags));
        sb.append(',');
        sb.append("datePluginDisable");
        sb.append('=');
        sb.append(((this.datePluginDisable == null)?"<null>":this.datePluginDisable));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.descData == null)? 0 :this.descData.hashCode()));
        result = ((result* 31)+((this.idTags == null)? 0 :this.idTags.hashCode()));
        result = ((result* 31)+((this.pinned == null)? 0 :this.pinned.hashCode()));
        result = ((result* 31)+((this.labelNames == null)? 0 :this.labelNames.hashCode()));
        result = ((result* 31)+((this.shortUrl == null)? 0 :this.shortUrl.hashCode()));
        result = ((result* 31)+((this.invited == null)? 0 :this.invited.hashCode()));
        result = ((result* 31)+((this.dateLastActivity == null)? 0 :this.dateLastActivity.hashCode()));
        result = ((result* 31)+((this.datePluginDisable == null)? 0 :this.datePluginDisable.hashCode()));
        result = ((result* 31)+((this.shortLink == null)? 0 :this.shortLink.hashCode()));
        result = ((result* 31)+((this.memberships == null)? 0 :this.memberships.hashCode()));
        result = ((result* 31)+((this.url == null)? 0 :this.url.hashCode()));
        result = ((result* 31)+((this.prefs == null)? 0 :this.prefs.hashCode()));
        result = ((result* 31)+((this.subscribed == null)? 0 :this.subscribed.hashCode()));
        result = ((result* 31)+((this.starred == null)? 0 :this.starred.hashCode()));
        result = ((result* 31)+((this.invitations == null)? 0 :this.invitations.hashCode()));
        result = ((result* 31)+((this.name == null)? 0 :this.name.hashCode()));
        result = ((result* 31)+((this.idOrganization == null)? 0 :this.idOrganization.hashCode()));
        result = ((result* 31)+((this.dateLastView == null)? 0 :this.dateLastView.hashCode()));
        result = ((result* 31)+((this.closed == null)? 0 :this.closed.hashCode()));
        result = ((result* 31)+((this.id == null)? 0 :this.id.hashCode()));
        result = ((result* 31)+((this.limits == null)? 0 :this.limits.hashCode()));
        result = ((result* 31)+((this.desc == null)? 0 :this.desc.hashCode()));
        result = ((result* 31)+((this.powerUps == null)? 0 :this.powerUps.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof TrelloBoard) == false) {
            return false;
        }
        TrelloBoard rhs = ((TrelloBoard) other);
        return ((((((((((((((((((((((((this.descData == rhs.descData)||((this.descData!= null)&&this.descData.equals(rhs.descData)))&&((this.idTags == rhs.idTags)||((this.idTags!= null)&&this.idTags.equals(rhs.idTags))))&&((this.pinned == rhs.pinned)||((this.pinned!= null)&&this.pinned.equals(rhs.pinned))))&&((this.labelNames == rhs.labelNames)||((this.labelNames!= null)&&this.labelNames.equals(rhs.labelNames))))&&((this.shortUrl == rhs.shortUrl)||((this.shortUrl!= null)&&this.shortUrl.equals(rhs.shortUrl))))&&((this.invited == rhs.invited)||((this.invited!= null)&&this.invited.equals(rhs.invited))))&&((this.dateLastActivity == rhs.dateLastActivity)||((this.dateLastActivity!= null)&&this.dateLastActivity.equals(rhs.dateLastActivity))))&&((this.datePluginDisable == rhs.datePluginDisable)||((this.datePluginDisable!= null)&&this.datePluginDisable.equals(rhs.datePluginDisable))))&&((this.shortLink == rhs.shortLink)||((this.shortLink!= null)&&this.shortLink.equals(rhs.shortLink))))&&((this.memberships == rhs.memberships)||((this.memberships!= null)&&this.memberships.equals(rhs.memberships))))&&((this.url == rhs.url)||((this.url!= null)&&this.url.equals(rhs.url))))&&((this.prefs == rhs.prefs)||((this.prefs!= null)&&this.prefs.equals(rhs.prefs))))&&((this.subscribed == rhs.subscribed)||((this.subscribed!= null)&&this.subscribed.equals(rhs.subscribed))))&&((this.starred == rhs.starred)||((this.starred!= null)&&this.starred.equals(rhs.starred))))&&((this.invitations == rhs.invitations)||((this.invitations!= null)&&this.invitations.equals(rhs.invitations))))&&((this.name == rhs.name)||((this.name!= null)&&this.name.equals(rhs.name))))&&((this.idOrganization == rhs.idOrganization)||((this.idOrganization!= null)&&this.idOrganization.equals(rhs.idOrganization))))&&((this.dateLastView == rhs.dateLastView)||((this.dateLastView!= null)&&this.dateLastView.equals(rhs.dateLastView))))&&((this.closed == rhs.closed)||((this.closed!= null)&&this.closed.equals(rhs.closed))))&&((this.id == rhs.id)||((this.id!= null)&&this.id.equals(rhs.id))))&&((this.limits == rhs.limits)||((this.limits!= null)&&this.limits.equals(rhs.limits))))&&((this.desc == rhs.desc)||((this.desc!= null)&&this.desc.equals(rhs.desc))))&&((this.powerUps == rhs.powerUps)||((this.powerUps!= null)&&this.powerUps.equals(rhs.powerUps))));
    }

}
