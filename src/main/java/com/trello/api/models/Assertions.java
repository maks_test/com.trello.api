package com.trello.api.models;

/**
 * Entry point for assertions of different data types. Each method in this class is a static factory for the
 * type-specific assertion objects.
 */
@javax.annotation.Generated(value = "assertj-assertions-generator")
public class Assertions {

    /**
     * Creates a new instance of <code>{@link com.trello.api.models.AttachmentsAssert}</code>.
     *
     * @param actual the actual value.
     * @return the created assertion object.
     */
    @org.assertj.core.util.CheckReturnValue
    public static com.trello.api.models.AttachmentsAssert assertThat(com.trello.api.models.Attachments actual) {
        return new com.trello.api.models.AttachmentsAssert(actual);
    }

    /**
     * Creates a new instance of <code>{@link com.trello.api.models.BackgroundImageScaledAssert}</code>.
     *
     * @param actual the actual value.
     * @return the created assertion object.
     */
    @org.assertj.core.util.CheckReturnValue
    public static com.trello.api.models.BackgroundImageScaledAssert assertThat(com.trello.api.models.BackgroundImageScaled actual) {
        return new com.trello.api.models.BackgroundImageScaledAssert(actual);
    }

    /**
     * Creates a new instance of <code>{@link com.trello.api.models.BoardsAssert}</code>.
     *
     * @param actual the actual value.
     * @return the created assertion object.
     */
    @org.assertj.core.util.CheckReturnValue
    public static com.trello.api.models.BoardsAssert assertThat(com.trello.api.models.Boards actual) {
        return new com.trello.api.models.BoardsAssert(actual);
    }

    /**
     * Creates a new instance of <code>{@link com.trello.api.models.CardsAssert}</code>.
     *
     * @param actual the actual value.
     * @return the created assertion object.
     */
    @org.assertj.core.util.CheckReturnValue
    public static com.trello.api.models.CardsAssert assertThat(com.trello.api.models.Cards actual) {
        return new com.trello.api.models.CardsAssert(actual);
    }

    /**
     * Creates a new instance of <code>{@link com.trello.api.models.ChecklistsAssert}</code>.
     *
     * @param actual the actual value.
     * @return the created assertion object.
     */
    @org.assertj.core.util.CheckReturnValue
    public static com.trello.api.models.ChecklistsAssert assertThat(com.trello.api.models.Checklists actual) {
        return new com.trello.api.models.ChecklistsAssert(actual);
    }

    /**
     * Creates a new instance of <code>{@link com.trello.api.models.CreateBoardModelAssert}</code>.
     *
     * @param actual the actual value.
     * @return the created assertion object.
     */
    @org.assertj.core.util.CheckReturnValue
    public static com.trello.api.models.CreateBoardModelAssert assertThat(com.trello.api.models.CreateBoardModel actual) {
        return new com.trello.api.models.CreateBoardModelAssert(actual);
    }

    /**
     * Creates a new instance of <code>{@link com.trello.api.models.CreateBoardModelBuilderAssert}</code>.
     *
     * @param actual the actual value.
     * @return the created assertion object.
     */
    @org.assertj.core.util.CheckReturnValue
    public static com.trello.api.models.CreateBoardModelBuilderAssert assertThat(com.trello.api.models.CreateBoardModel.Builder actual) {
        return new com.trello.api.models.CreateBoardModelBuilderAssert(actual);
    }

    /**
     * Creates a new instance of <code>{@link com.trello.api.models.CustomFieldsAssert}</code>.
     *
     * @param actual the actual value.
     * @return the created assertion object.
     */
    @org.assertj.core.util.CheckReturnValue
    public static com.trello.api.models.CustomFieldsAssert assertThat(com.trello.api.models.CustomFields actual) {
        return new com.trello.api.models.CustomFieldsAssert(actual);
    }

    /**
     * Creates a new instance of <code>{@link com.trello.api.models.InvalidIdAssert}</code>.
     *
     * @param actual the actual value.
     * @return the created assertion object.
     */
    @org.assertj.core.util.CheckReturnValue
    public static com.trello.api.models.InvalidIdAssert assertThat(com.trello.api.models.InvalidId actual) {
        return new com.trello.api.models.InvalidIdAssert(actual);
    }

    /**
     * Creates a new instance of <code>{@link com.trello.api.models.LabelNamesAssert}</code>.
     *
     * @param actual the actual value.
     * @return the created assertion object.
     */
    @org.assertj.core.util.CheckReturnValue
    public static com.trello.api.models.LabelNamesAssert assertThat(com.trello.api.models.LabelNames actual) {
        return new com.trello.api.models.LabelNamesAssert(actual);
    }

    /**
     * Creates a new instance of <code>{@link com.trello.api.models.LabelsAssert}</code>.
     *
     * @param actual the actual value.
     * @return the created assertion object.
     */
    @org.assertj.core.util.CheckReturnValue
    public static com.trello.api.models.LabelsAssert assertThat(com.trello.api.models.Labels actual) {
        return new com.trello.api.models.LabelsAssert(actual);
    }

    /**
     * Creates a new instance of <code>{@link com.trello.api.models.LimitsAssert}</code>.
     *
     * @param actual the actual value.
     * @return the created assertion object.
     */
    @org.assertj.core.util.CheckReturnValue
    public static com.trello.api.models.LimitsAssert assertThat(com.trello.api.models.Limits actual) {
        return new com.trello.api.models.LimitsAssert(actual);
    }

    /**
     * Creates a new instance of <code>{@link com.trello.api.models.ListsAssert}</code>.
     *
     * @param actual the actual value.
     * @return the created assertion object.
     */
    @org.assertj.core.util.CheckReturnValue
    public static com.trello.api.models.ListsAssert assertThat(com.trello.api.models.Lists actual) {
        return new com.trello.api.models.ListsAssert(actual);
    }

    /**
     * Creates a new instance of <code>{@link com.trello.api.models.MembershipAssert}</code>.
     *
     * @param actual the actual value.
     * @return the created assertion object.
     */
    @org.assertj.core.util.CheckReturnValue
    public static com.trello.api.models.MembershipAssert assertThat(com.trello.api.models.Membership actual) {
        return new com.trello.api.models.MembershipAssert(actual);
    }

    /**
     * Creates a new instance of <code>{@link com.trello.api.models.OpenPerBoardAssert}</code>.
     *
     * @param actual the actual value.
     * @return the created assertion object.
     */
    @org.assertj.core.util.CheckReturnValue
    public static com.trello.api.models.OpenPerBoardAssert assertThat(com.trello.api.models.OpenPerBoard actual) {
        return new com.trello.api.models.OpenPerBoardAssert(actual);
    }

    /**
     * Creates a new instance of <code>{@link com.trello.api.models.PerBoardAssert}</code>.
     *
     * @param actual the actual value.
     * @return the created assertion object.
     */
    @org.assertj.core.util.CheckReturnValue
    public static com.trello.api.models.PerBoardAssert assertThat(com.trello.api.models.PerBoard actual) {
        return new com.trello.api.models.PerBoardAssert(actual);
    }

    /**
     * Creates a new instance of <code>{@link com.trello.api.models.PrefsAssert}</code>.
     *
     * @param actual the actual value.
     * @return the created assertion object.
     */
    @org.assertj.core.util.CheckReturnValue
    public static com.trello.api.models.PrefsAssert assertThat(com.trello.api.models.Prefs actual) {
        return new com.trello.api.models.PrefsAssert(actual);
    }

    /**
     * Creates a new instance of <code>{@link com.trello.api.models.TotalMembersPerBoardAssert}</code>.
     *
     * @param actual the actual value.
     * @return the created assertion object.
     */
    @org.assertj.core.util.CheckReturnValue
    public static com.trello.api.models.TotalMembersPerBoardAssert assertThat(com.trello.api.models.TotalMembersPerBoard actual) {
        return new com.trello.api.models.TotalMembersPerBoardAssert(actual);
    }

    /**
     * Creates a new instance of <code>{@link com.trello.api.models.TotalPerBoardAssert}</code>.
     *
     * @param actual the actual value.
     * @return the created assertion object.
     */
    @org.assertj.core.util.CheckReturnValue
    public static com.trello.api.models.TotalPerBoardAssert assertThat(com.trello.api.models.TotalPerBoard actual) {
        return new com.trello.api.models.TotalPerBoardAssert(actual);
    }

    /**
     * Creates a new instance of <code>{@link com.trello.api.models.TrelloBoardAssert}</code>.
     *
     * @param actual the actual value.
     * @return the created assertion object.
     */
    @org.assertj.core.util.CheckReturnValue
    public static com.trello.api.models.TrelloBoardAssert assertThat(com.trello.api.models.TrelloBoard actual) {
        return new com.trello.api.models.TrelloBoardAssert(actual);
    }

    /**
     * Creates a new <code>{@link Assertions}</code>.
     */
    protected Assertions() {
        // empty
    }
}
