package com.trello.api.controllers;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.specification.RequestSpecification;
import org.hamcrest.Matchers;

public class BaseController {
    public static RequestSpecification requestSpecification;

    public BaseController() {
        String baseUri = "https://api.trello.com/1";
        String basePath = "boards";
        String key = "baa0fcad2719638c39f2727007878daf";
        String token = "a0e61cdedd02cba4f7fe3d1496b3bb48232def78808a5fb519681d5fba49747b";

        requestSpecification = new RequestSpecBuilder()
                .setBaseUri(baseUri)
                .setBasePath(basePath)
                .addQueryParam("key", key)
                .addQueryParam("token", token)
                .setAccept(ContentType.JSON)
                .setContentType(ContentType.JSON)
                .log(LogDetail.ALL).build();

        RestAssured.responseSpecification = new ResponseSpecBuilder()
                .expectContentType(ContentType.JSON)
                .expectResponseTime(Matchers.lessThan(15000L))
                .build();

        RestAssured.defaultParser = Parser.JSON;
    }
}
