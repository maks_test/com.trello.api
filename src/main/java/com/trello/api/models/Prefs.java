
package com.trello.api.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "permissionLevel",
        "voting",
        "comments",
        "invitations",
        "selfJoin",
        "cardCovers",
        "cardAging",
        "calendarFeedEnabled",
        "background",
        "backgroundImage",
        "backgroundImageScaled",
        "backgroundTile",
        "backgroundBrightness",
        "backgroundColor",
        "backgroundBottomColor",
        "backgroundTopColor",
        "canBePublic",
        "canBeOrg",
        "canBePrivate",
        "canInvite"
})
public class Prefs {

    @JsonProperty("permissionLevel")
    private String permissionLevel;
    @JsonProperty("voting")
    private String voting;
    @JsonProperty("comments")
    private String comments;
    @JsonProperty("invitations")
    private String invitations;
    @JsonProperty("selfJoin")
    private Boolean selfJoin;
    @JsonProperty("cardCovers")
    private Boolean cardCovers;
    @JsonProperty("cardAging")
    private String cardAging;
    @JsonProperty("calendarFeedEnabled")
    private Boolean calendarFeedEnabled;
    @JsonProperty("background")
    private String background;
    @JsonProperty("backgroundImage")
    private String backgroundImage;
    @JsonProperty("backgroundImageScaled")
    private List<BackgroundImageScaled> backgroundImageScaled = new ArrayList<BackgroundImageScaled>();
    @JsonProperty("backgroundTile")
    private Boolean backgroundTile;
    @JsonProperty("backgroundBrightness")
    private String backgroundBrightness;
    @JsonProperty("backgroundColor")
    private String backgroundColor;
    @JsonProperty("backgroundBottomColor")
    private String backgroundBottomColor;
    @JsonProperty("backgroundTopColor")
    private String backgroundTopColor;
    @JsonProperty("canBePublic")
    private Boolean canBePublic;
    @JsonProperty("canBeOrg")
    private Boolean canBeOrg;
    @JsonProperty("canBePrivate")
    private Boolean canBePrivate;
    @JsonProperty("canInvite")
    private Boolean canInvite;

    @JsonProperty("backgroundColor")
    public String getBackgroundColor() {
        return backgroundColor;
    }

    @JsonProperty("backgroundColor")
    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    @JsonProperty("permissionLevel")
    public String getPermissionLevel() {
        return permissionLevel;
    }

    @JsonProperty("permissionLevel")
    public void setPermissionLevel(String permissionLevel) {
        this.permissionLevel = permissionLevel;
    }

    @JsonProperty("voting")
    public String getVoting() {
        return voting;
    }

    @JsonProperty("voting")
    public void setVoting(String voting) {
        this.voting = voting;
    }

    @JsonProperty("comments")
    public String getComments() {
        return comments;
    }

    @JsonProperty("comments")
    public void setComments(String comments) {
        this.comments = comments;
    }

    @JsonProperty("invitations")
    public String getInvitations() {
        return invitations;
    }

    @JsonProperty("invitations")
    public void setInvitations(String invitations) {
        this.invitations = invitations;
    }

    @JsonProperty("selfJoin")
    public Boolean getSelfJoin() {
        return selfJoin;
    }

    @JsonProperty("selfJoin")
    public void setSelfJoin(Boolean selfJoin) {
        this.selfJoin = selfJoin;
    }

    @JsonProperty("cardCovers")
    public Boolean getCardCovers() {
        return cardCovers;
    }

    @JsonProperty("cardCovers")
    public void setCardCovers(Boolean cardCovers) {
        this.cardCovers = cardCovers;
    }

    @JsonProperty("cardAging")
    public String getCardAging() {
        return cardAging;
    }

    @JsonProperty("cardAging")
    public void setCardAging(String cardAging) {
        this.cardAging = cardAging;
    }

    @JsonProperty("calendarFeedEnabled")
    public Boolean getCalendarFeedEnabled() {
        return calendarFeedEnabled;
    }

    @JsonProperty("calendarFeedEnabled")
    public void setCalendarFeedEnabled(Boolean calendarFeedEnabled) {
        this.calendarFeedEnabled = calendarFeedEnabled;
    }

    @JsonProperty("background")
    public String getBackground() {
        return background;
    }

    @JsonProperty("background")
    public void setBackground(String background) {
        this.background = background;
    }

    @JsonProperty("backgroundImage")
    public String getBackgroundImage() {
        return backgroundImage;
    }

    @JsonProperty("backgroundImage")
    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    @JsonProperty("backgroundImageScaled")
    public List<BackgroundImageScaled> getBackgroundImageScaled() {
        return backgroundImageScaled;
    }

    @JsonProperty("backgroundImageScaled")
    public void setBackgroundImageScaled(List<BackgroundImageScaled> backgroundImageScaled) {
        this.backgroundImageScaled = backgroundImageScaled;
    }

    @JsonProperty("backgroundTile")
    public Boolean getBackgroundTile() {
        return backgroundTile;
    }

    @JsonProperty("backgroundTile")
    public void setBackgroundTile(Boolean backgroundTile) {
        this.backgroundTile = backgroundTile;
    }

    @JsonProperty("backgroundBrightness")
    public String getBackgroundBrightness() {
        return backgroundBrightness;
    }

    @JsonProperty("backgroundBrightness")
    public void setBackgroundBrightness(String backgroundBrightness) {
        this.backgroundBrightness = backgroundBrightness;
    }

    @JsonProperty("backgroundBottomColor")
    public String getBackgroundBottomColor() {
        return backgroundBottomColor;
    }

    @JsonProperty("backgroundBottomColor")
    public void setBackgroundBottomColor(String backgroundBottomColor) {
        this.backgroundBottomColor = backgroundBottomColor;
    }

    @JsonProperty("backgroundTopColor")
    public String getBackgroundTopColor() {
        return backgroundTopColor;
    }

    @JsonProperty("backgroundTopColor")
    public void setBackgroundTopColor(String backgroundTopColor) {
        this.backgroundTopColor = backgroundTopColor;
    }

    @JsonProperty("canBePublic")
    public Boolean getCanBePublic() {
        return canBePublic;
    }

    @JsonProperty("canBePublic")
    public void setCanBePublic(Boolean canBePublic) {
        this.canBePublic = canBePublic;
    }

    @JsonProperty("canBeOrg")
    public Boolean getCanBeOrg() {
        return canBeOrg;
    }

    @JsonProperty("canBeOrg")
    public void setCanBeOrg(Boolean canBeOrg) {
        this.canBeOrg = canBeOrg;
    }

    @JsonProperty("canBePrivate")
    public Boolean getCanBePrivate() {
        return canBePrivate;
    }

    @JsonProperty("canBePrivate")
    public void setCanBePrivate(Boolean canBePrivate) {
        this.canBePrivate = canBePrivate;
    }

    @JsonProperty("canInvite")
    public Boolean getCanInvite() {
        return canInvite;
    }

    @JsonProperty("canInvite")
    public void setCanInvite(Boolean canInvite) {
        this.canInvite = canInvite;
    }

    @Override
    public String toString() {
        return "Prefs{" +
                "permissionLevel='" + permissionLevel + '\'' +
                ", voting='" + voting + '\'' +
                ", comments='" + comments + '\'' +
                ", invitations='" + invitations + '\'' +
                ", selfJoin=" + selfJoin +
                ", cardCovers=" + cardCovers +
                ", cardAging='" + cardAging + '\'' +
                ", calendarFeedEnabled=" + calendarFeedEnabled +
                ", background='" + background + '\'' +
                ", backgroundImage='" + backgroundImage + '\'' +
                ", backgroundImageScaled=" + backgroundImageScaled +
                ", backgroundTile=" + backgroundTile +
                ", backgroundBrightness='" + backgroundBrightness + '\'' +
                ", backgroundColor='" + backgroundColor + '\'' +
                ", backgroundBottomColor='" + backgroundBottomColor + '\'' +
                ", backgroundTopColor='" + backgroundTopColor + '\'' +
                ", canBePublic=" + canBePublic +
                ", canBeOrg=" + canBeOrg +
                ", canBePrivate=" + canBePrivate +
                ", canInvite=" + canInvite +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Prefs prefs = (Prefs) o;

        if (getPermissionLevel() != null ? !getPermissionLevel().equals(prefs.getPermissionLevel()) : prefs.getPermissionLevel() != null)
            return false;
        if (getVoting() != null ? !getVoting().equals(prefs.getVoting()) : prefs.getVoting() != null) return false;
        if (getComments() != null ? !getComments().equals(prefs.getComments()) : prefs.getComments() != null)
            return false;
        if (getInvitations() != null ? !getInvitations().equals(prefs.getInvitations()) : prefs.getInvitations() != null)
            return false;
        if (getSelfJoin() != null ? !getSelfJoin().equals(prefs.getSelfJoin()) : prefs.getSelfJoin() != null)
            return false;
        if (getCardCovers() != null ? !getCardCovers().equals(prefs.getCardCovers()) : prefs.getCardCovers() != null)
            return false;
        if (getCardAging() != null ? !getCardAging().equals(prefs.getCardAging()) : prefs.getCardAging() != null)
            return false;
        if (getCalendarFeedEnabled() != null ? !getCalendarFeedEnabled().equals(prefs.getCalendarFeedEnabled()) : prefs.getCalendarFeedEnabled() != null)
            return false;
        if (getBackground() != null ? !getBackground().equals(prefs.getBackground()) : prefs.getBackground() != null)
            return false;
        if (getBackgroundImage() != null ? !getBackgroundImage().equals(prefs.getBackgroundImage()) : prefs.getBackgroundImage() != null)
            return false;
        if (getBackgroundImageScaled() != null ? !getBackgroundImageScaled().equals(prefs.getBackgroundImageScaled()) : prefs.getBackgroundImageScaled() != null)
            return false;
        if (getBackgroundTile() != null ? !getBackgroundTile().equals(prefs.getBackgroundTile()) : prefs.getBackgroundTile() != null)
            return false;
        if (getBackgroundBrightness() != null ? !getBackgroundBrightness().equals(prefs.getBackgroundBrightness()) : prefs.getBackgroundBrightness() != null)
            return false;
        if (getBackgroundColor() != null ? !getBackgroundColor().equals(prefs.getBackgroundColor()) : prefs.getBackgroundColor() != null)
            return false;
        if (getBackgroundBottomColor() != null ? !getBackgroundBottomColor().equals(prefs.getBackgroundBottomColor()) : prefs.getBackgroundBottomColor() != null)
            return false;
        if (getBackgroundTopColor() != null ? !getBackgroundTopColor().equals(prefs.getBackgroundTopColor()) : prefs.getBackgroundTopColor() != null)
            return false;
        if (getCanBePublic() != null ? !getCanBePublic().equals(prefs.getCanBePublic()) : prefs.getCanBePublic() != null)
            return false;
        if (getCanBeOrg() != null ? !getCanBeOrg().equals(prefs.getCanBeOrg()) : prefs.getCanBeOrg() != null)
            return false;
        if (getCanBePrivate() != null ? !getCanBePrivate().equals(prefs.getCanBePrivate()) : prefs.getCanBePrivate() != null)
            return false;
        return getCanInvite() != null ? getCanInvite().equals(prefs.getCanInvite()) : prefs.getCanInvite() == null;
    }

    @Override
    public int hashCode() {
        int result = getPermissionLevel() != null ? getPermissionLevel().hashCode() : 0;
        result = 31 * result + (getVoting() != null ? getVoting().hashCode() : 0);
        result = 31 * result + (getComments() != null ? getComments().hashCode() : 0);
        result = 31 * result + (getInvitations() != null ? getInvitations().hashCode() : 0);
        result = 31 * result + (getSelfJoin() != null ? getSelfJoin().hashCode() : 0);
        result = 31 * result + (getCardCovers() != null ? getCardCovers().hashCode() : 0);
        result = 31 * result + (getCardAging() != null ? getCardAging().hashCode() : 0);
        result = 31 * result + (getCalendarFeedEnabled() != null ? getCalendarFeedEnabled().hashCode() : 0);
        result = 31 * result + (getBackground() != null ? getBackground().hashCode() : 0);
        result = 31 * result + (getBackgroundImage() != null ? getBackgroundImage().hashCode() : 0);
        result = 31 * result + (getBackgroundImageScaled() != null ? getBackgroundImageScaled().hashCode() : 0);
        result = 31 * result + (getBackgroundTile() != null ? getBackgroundTile().hashCode() : 0);
        result = 31 * result + (getBackgroundBrightness() != null ? getBackgroundBrightness().hashCode() : 0);
        result = 31 * result + (getBackgroundColor() != null ? getBackgroundColor().hashCode() : 0);
        result = 31 * result + (getBackgroundBottomColor() != null ? getBackgroundBottomColor().hashCode() : 0);
        result = 31 * result + (getBackgroundTopColor() != null ? getBackgroundTopColor().hashCode() : 0);
        result = 31 * result + (getCanBePublic() != null ? getCanBePublic().hashCode() : 0);
        result = 31 * result + (getCanBeOrg() != null ? getCanBeOrg().hashCode() : 0);
        result = 31 * result + (getCanBePrivate() != null ? getCanBePrivate().hashCode() : 0);
        result = 31 * result + (getCanInvite() != null ? getCanInvite().hashCode() : 0);
        return result;
    }
}
